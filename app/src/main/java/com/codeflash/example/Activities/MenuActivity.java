package com.codeflash.example.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.codeflash.example.Fragments.MyMapsFragment;
import com.codeflash.example.R;
import com.codeflash.example.example;

public class MenuActivity extends AppCompatActivity {

    /**
     * Instanciaciones de views
     */
    private Button btnLogin,btnRegister, btnMap, btnFacebook;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        /**
         *
         * Asignación de variables
         */
        initComponents();
        /**
         *
         * Redireccionamiento MainActivity la vista de login
         */
        redirectLogin();
        /**
         *
         * Redireccionamiento MainActivity la vista de registro
         */
        redirectRegister();
        /**
         *
         * Redireccionamiento MainActivity la vista del mapa
         */
        redirectMap();

        /**
         *
         * Redireccionamiento MainActivity la vista del facebook
         */
        redirectExample();
    }

    private void redirectExample() {
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void redirectMap() {
        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intentRegister = new Intent(MenuActivity.this, MainActivity.class);
                startActivity(intentRegister);
                finish();
            }
        });
    }

    private void redirectRegister() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intentRegister = new Intent(MenuActivity.this, RegisterActivity.class);
                startActivity(intentRegister);
            }
        });
    }

    private void redirectLogin() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intentLogin = new Intent(MenuActivity.this, LoginActivity.class);
                startActivity(intentLogin);
            }
        });
    }

    private void initComponents() {
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnMap = (Button) findViewById(R.id.btnMap);
        btnFacebook = (Button) findViewById(R.id.btnFacebook);
    }


}
