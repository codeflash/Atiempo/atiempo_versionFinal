package com.codeflash.example.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.codeflash.example.Fragments.MyMapsFragment;
import com.codeflash.example.R;

public class TransitionMapsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transition_maps);
        new Handler().postDelayed(new Runnable() {

            // Using handler with postDelayed called runnable run method

            @Override
            public void run() {
                    Intent i = new Intent(TransitionMapsActivity.this, MainActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, 500); // wait for 5 seconds

    }
}
