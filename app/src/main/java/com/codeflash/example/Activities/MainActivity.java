package com.codeflash.example.Activities;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.codeflash.example.Adapters.AdapterDrawer;
import com.codeflash.example.Fragments.MyMapsFragment;
import com.codeflash.example.Models.DataDrawer;
import com.codeflash.example.R;
import com.codeflash.example.example;
import com.google.android.gms.maps.MapFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ListView.OnItemClickListener{
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private ListView listView;
    private AdapterDrawer adapterDrwer;
    private List<DataDrawer> dataDrawers;
    private boolean isLunch = true;
    private int currentPosition = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initComponentsDrawer();
        setDataDrawers();
        configToolbar();
        initNavigationDrawer();
        if(isLunch){

            getFragment(currentPosition);
            isLunch = false;
        }


    }

    private void setDataDrawers() {
        dataDrawers.add(new DataDrawer(getResources().getString(R.string.navigation_home), "" , false));
        dataDrawers.add(new DataDrawer(getResources().getString(R.string.navigation_myweek), "" , false));
        dataDrawers.add(new DataDrawer(getResources().getString(R.string.navigation_recipes), "" , false));
        dataDrawers.add(new DataDrawer(getResources().getString(R.string.navigation_supplies), "" , false));
    }

    private void initComponentsDrawer(){
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer);
        listView = (ListView) findViewById(R.id.drawer_list);
        dataDrawers = new ArrayList<>();
        adapterDrwer = new AdapterDrawer(getBaseContext());
    }

    private void configToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    private void initNavigationDrawer() {
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.header_navigation_drawer, listView, false);
        listView.addHeaderView(header, null, false);
        adapterDrwer.setDataDrawers(dataDrawers);
        listView.setAdapter(adapterDrwer);
        listView.setOnItemClickListener(this);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getFragment(position);
    }

    protected void getFragment(int position) {
        Fragment fragment = null;
        switch (position) {
            case 1:
                if (!(currentPosition == position) || isLunch){
                    fragment = new example();
                }
                break;
            default:
                fragment =  new example();
                break;
        }

        if(fragment != null){
            getFragmentManager().beginTransaction().replace(R.id.main, fragment).commit();

        }
        drawerLayout.closeDrawers();

    }




}