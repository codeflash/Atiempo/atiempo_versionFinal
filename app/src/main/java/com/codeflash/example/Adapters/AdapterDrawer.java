package com.codeflash.example.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.codeflash.example.Models.DataDrawer;
import com.codeflash.example.R;

import java.util.List;


public class AdapterDrawer extends BaseAdapter {

    private Context context;
    private List<DataDrawer> dataDrawers;
    private TextView title;
    private TextView count;

    public AdapterDrawer(Context context, List<DataDrawer> dataDrawers) {
        this.context = context;
        this.dataDrawers = dataDrawers;
    }

    public AdapterDrawer(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return dataDrawers.size();
    }

    @Override
    public Object getItem(int position) {
        return dataDrawers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_navigation_drawer, null);
        }
        initComponents(convertView);
        setComponents(position);
        return convertView;
    }

    private void setComponents(int position) {
        title.setText(dataDrawers.get(position).getTitle());
        if(dataDrawers.get(position).isCountVisibility()){
            count.setText(dataDrawers.get(position).getCount());
        }else{
            count.setVisibility(View.GONE);
        }

    }

    private void initComponents(View convertView) {
        title = (TextView) convertView.findViewById(R.id.title);
        count = (TextView) convertView.findViewById(R.id.count);
    }

    public List<DataDrawer> getDataDrawers() {
        return dataDrawers;
    }

    public void setDataDrawers(List<DataDrawer> dataDrawers) {
        this.dataDrawers = dataDrawers;
    }
}
