package com.codeflash.example.Util;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.codeflash.example.Adapters.PlaceAutoCompleteAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by luigi on 29/12/16.
 */

public class SearchPlaces extends FragmentActivity{

    private static GoogleApiClient googleApiClient;
    private PlaceAutoCompleteAdapter placeAutoCompleteAdapter;
    private AutoCompleteTextView autoCompleteTextView;
    private AutocompleteFilter filter;
    private LatLngBounds latLngBounds;
    private AppCompatActivity activity;
    private ResultCallback<PlaceBuffer> updatePlaceDetailsCallback;

    public SearchPlaces(AppCompatActivity activity) {
        this.activity = activity;
    }

    public SearchPlaces(AppCompatActivity activity , AutoCompleteTextView autoCompleteTextView) {
        this.activity = activity;
        this.autoCompleteTextView = autoCompleteTextView;
    }

    public void setLatLngBounds(LatLngBounds latLngBounds) {
        this.latLngBounds = latLngBounds;
    }

    public void setFilter(AutocompleteFilter filter) {
        this.filter = filter;
    }

    public static void setGoogleApiClient(GoogleApiClient mGgoogleApiClient) {
        googleApiClient = mGgoogleApiClient;
    }



    public static GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }


    public void setPlaceAutoCompleteAdapter(PlaceAutoCompleteAdapter placeAutoCompleteAdapter) {
        this.placeAutoCompleteAdapter = placeAutoCompleteAdapter;
    }


    public void setAutoCompleteTextView(AutoCompleteTextView autoCompleteTextView) {
        this.autoCompleteTextView = autoCompleteTextView;
    }

    public void setUpdatePlaceDetailsCallback(ResultCallback<PlaceBuffer> updatePlaceDetailsCallback) {
        this.updatePlaceDetailsCallback = updatePlaceDetailsCallback;
    }



    public void init(){

        if(latLngBounds == null){
            latLngBounds = new LatLngBounds(
                                new LatLng(-34.041458, 150.790100),
                                new LatLng(-33.682247, 151.383362));
        }
        if(filter == null){
            filter   =  new AutocompleteFilter.Builder().setCountry("PE").build();
        }
        if(placeAutoCompleteAdapter == null) {
            placeAutoCompleteAdapter = new PlaceAutoCompleteAdapter(
                    activity,
                    googleApiClient,
                    latLngBounds,
                    filter);
        }
        autoCompleteTextView.setAdapter(placeAutoCompleteAdapter);
        autoCompleteTextView.setOnItemClickListener(autocompleteClickListener);
    }


    private AdapterView.OnItemClickListener autocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
                 Retrieve the place ID of the selected item from the Adapter.
                 The adapter stores each Place suggestion in MainActivity AutocompletePrediction from which we
                 read the place ID and title.
                  */
            final AutocompletePrediction item = placeAutoCompleteAdapter.getItem(position);
            final String placeId = item.getPlaceId();

            /*
             Issue MainActivity request to the Places Geo Data API to retrieve MainActivity Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(googleApiClient, placeId);
            placeResult.setResultCallback(updatePlaceDetailsCallback);

            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

            inputMethodManager.hideSoftInputFromWindow(autoCompleteTextView.getWindowToken(), 0);

            Log.i("asdsad", "Called getPlaceById to get Place details for " + placeId);
        }
    };





}
