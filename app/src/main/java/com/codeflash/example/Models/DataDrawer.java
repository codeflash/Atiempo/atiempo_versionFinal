package com.codeflash.example.Models;


public class DataDrawer {

    private String title;
    private String count;
    private boolean countVisibility;

    public DataDrawer() {}

    public DataDrawer(String title ,String count ,boolean countVisibility) {
        this.title = title;
        this.count = count;
        this.countVisibility = countVisibility;
    }

    public String getTitle() {
        return title;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCountVisibility() {
        return countVisibility;
    }

    public void setCountVisibility(boolean countVisibility) {
        this.countVisibility = countVisibility;
    }
}
