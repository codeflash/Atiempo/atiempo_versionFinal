package com.codeflash.example.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.codeflash.example.Activities.MainActivity;
import com.codeflash.example.R;
import com.codeflash.example.Util.SearchPlaces;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Luigi on 13/01/2017.
 */

public class MyMapsFragment extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<Status> {

    /**
     * CONSTANTES
     */
    private static final String TAG_SEARCH = "Places Search";
    private static final String TAG = MyMapsFragment.class.getSimpleName();
    private static final String LOCATION_KEY = "location-key";
    public static final long UPDATE_INTERVAL = 1000;
    public static final long UPDATE_FASTEST_INTERVAL = UPDATE_INTERVAL / 2;

    /**
     * Boton flotante para mover la camara a mi posicion
     */
    private FloatingActionButton fabMyPosition;

    /**
     * Location API
     */
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private Location mLastLocation;

    /**
     * Códigos de petición
     */
    public static final int REQUEST_LOCATION = 1;
    public static final int REQUEST_CHECK_SETTINGS = 2;

    /**
     * Cliente del Api de google
     */
    private static GoogleApiClient mGoogleApiClient;

    /**
     * Componentes para poder borrar contenido de buscadores
     */
    Button clearButtonEnd,
            clearButtonStart;
    /**
     * Latitudes y longitudes marcadores
     */
    private LatLng startLatLng,
            endLatLng,
            defaultLatLng;

    /**
     * Marcadores para google maps
     */
    private Marker startMarker,
            endMarker;

    /**
     * google maps
     */
    private static GoogleMap mMap;

    /**
     * mostrar buscador de partida
     */
    private CheckBox mCheckBox;

    /**
     * Layout que contiene el buscador de la partida
     */
    private RelativeLayout StartSearch;

    /**
     * delimitador del area de busqueda
     */
    private LatLngBounds.Builder BoundsLatLng;

    /**
     * componentes para la busqueda de lugares
     */
    private AutoCompleteTextView mAutocompleteViewStart,
            mAutocompleteViewEnd;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_maps);

        /**
         * Referencias ui
         */
        initComponents();

        /**
         * procesps segundo plano
         */
        secondProcess();


    }


    private void initComponents() {
        mAutocompleteViewEnd = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);
        mAutocompleteViewStart = (AutoCompleteTextView) findViewById(R.id.autocomplete_places_start);
        mCheckBox = (CheckBox) findViewById(R.id.checkBox);
        StartSearch = (RelativeLayout) findViewById(R.id.StartSearch);
        clearButtonEnd = (Button) findViewById(R.id.btnClearEnd);
        clearButtonStart = (Button) findViewById(R.id.btnClearStart);
        fabMyPosition = (FloatingActionButton) findViewById(R.id.fabMyPosition);

    }


    private void secondProcess(){
        new Thread(new Runnable() {
            public void run() {
                try {
                    /**
                    * Configurando cliente de api de google
                    */
                    buildGoogleApiClient();

                    /**
                     * cargar mapa
                     */

                    final SupportMapFragment mapFragment =   SupportMapFragment.newInstance();
                    getSupportFragmentManager().beginTransaction().add(R.id.map , mapFragment).commit();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            /**
                             * obtener mapa
                             */
                            mapFragment.getMapAsync(MyMapsFragment.this);

                            /**
                             * Configurando buscadores
                             */
                            settingsSearchAutoCompleteTextView();

                            /**
                             * Crear configuracion de peticiones
                             */
                            createLocationRequest();

                            /**
                             * Crear opciones de peticiones
                             */
                            buildLocationSettingsRequest();

                            /**
                             * Verificar ajustes de ubicación actuales
                             */
                            checkLocationSettings();

                            /**
                             * escuchar al evento de borrar el buscador de lugares de partida
                             */
                            deleteTextSearchStart();

                            /**
                             * escuchar al evento de borrar el buscador de lugares de llegada
                             */
                            deleteTextSearchEnd();

                            /**
                             * Mostrar u ocultar el buscador de lugares de partida
                             */
                            toggleSearchStart();

                            /**
                             * mover la camara de google maps a mi posicion
                             */
                            moveToMyPosition();
                        }
                    });

                }catch (Exception ignored){}
            }
        }).start();
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .enableAutoManage(this, 0 , this)
                .build();
    }

    private void settingsSearchAutoCompleteTextView() {
        SearchPlaces.setGoogleApiClient(mGoogleApiClient);

        SearchPlaces searchPlacesStart = new SearchPlaces(this, mAutocompleteViewStart);
        searchPlacesStart.init();
        searchPlacesStart.setUpdatePlaceDetailsCallback(callbackSearchStart());

        SearchPlaces searchPlacesEnd = new SearchPlaces(this, mAutocompleteViewEnd);
        searchPlacesEnd.init();
        searchPlacesEnd.setUpdatePlaceDetailsCallback(callbackSearchEnd());
    }


    private ResultCallback<PlaceBuffer> callbackSearchEnd() {
        return new ResultCallback<PlaceBuffer>() {
            @Override
            public void onResult(PlaceBuffer places) {
                if (!places.getStatus().isSuccess()) {
                    /**
                     * Si la peticion no se cumplio satisfactoriamente
                     */
                    Log.e(TAG_SEARCH, "Place query did not complete. Error: " + places.getStatus().toString());
                    places.release();
                    return;
                }
                /**
                 * Extraer el lugar del buffer
                 */
                final Place place = places.get(0);

                /**
                 * Setear destino
                 */
                endLatLng = place.getLatLng();

                Log.d(TAG, endLatLng.latitude + "");

                if (endMarker != null) {
                    endMarker.remove();
                }
                endMarker = mMap.addMarker(new MarkerOptions().position(endLatLng));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(endLatLng, 16));

                places.release();
            }
        };
    }

    private ResultCallback<PlaceBuffer> callbackSearchStart() {
        return new ResultCallback<PlaceBuffer>() {
            @Override
            public void onResult(PlaceBuffer places) {
                if (!places.getStatus().isSuccess()) {
                    /**
                     * Si la peticion no se cumplio satisfactoriamente
                     */
                    Log.e(TAG_SEARCH, "Place query did not complete. Error: " + places.getStatus().toString());
                    places.release();
                    return;
                }
                /**
                 * Extraer el lugar del buffer
                 */
                final Place place = places.get(0);

                /**
                 * Setear destino
                 */
                startLatLng = place.getLatLng();
                if (startMarker != null) {
                    startMarker.remove();
                }
                startMarker = mMap.addMarker(new MarkerOptions().position(startLatLng));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLatLng, 16));

                places.release();
            }
        };
    }


    private void deleteTextSearchStart() {
        clearButtonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutocompleteViewStart.setText("");
                if (startMarker != null) {
                    startMarker.remove();
                }
            }
        });
    }

    private void deleteTextSearchEnd() {
        clearButtonEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutocompleteViewEnd.setText("");
                if (endMarker != null) {
                    endMarker.remove();
                }

            }
        });
    }

    private void toggleSearchStart() {
        /**
         * Motrar u ocultar el el buscador de lugares de partida
         */
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mAutocompleteViewStart.requestFocus();
                    StartSearch.setVisibility(View.VISIBLE);
                } else {
                    StartSearch.setVisibility(View.GONE);
                }
            }
        });
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest()
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(UPDATE_FASTEST_INTERVAL)
                .setSmallestDisplacement(10)
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest)
                .setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    private void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient, mLocationSettingsRequest
                );

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                Status status = result.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.d(TAG, "Los ajustes de ubicación satisfacen la configuración.");
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            Log.d(TAG, "Los ajustes de ubicación no satisfacen la configuración. " +
                                    "Se mostrará un diálogo de ayuda.");
                            status.startResolutionForResult(
                                    MyMapsFragment.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.d(TAG, "El Intent del diálogo no funcionó.");
                            // Sin operaciones
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.d(TAG, "Los ajustes de ubicación no son apropiados.");
                        break;

                }
            }
        });

    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi
                .removeLocationUpdates(mGoogleApiClient, this);
    }

    private void getLastLocation() {
        if (isLocationPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        } else {
            manageDeniedPermission();
        }
    }

    private void processLastLocation() {
        getLastLocation();
        if (mLastLocation != null) {
            updateMyLocation(mLastLocation);
        }
    }

    private void startLocationUpdates() {
        if (isLocationPermissionGranted()) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } else {
            manageDeniedPermission();
        }
    }

    private void moveToMyPosition() {

        fabMyPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLocationSettings();
                if (startMarker != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLatLng, 16));
                }
            }
        });
    }

    /**
     * Configurando Mapa
     */
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        mMap.getUiSettings().setMapToolbarEnabled(false);


//        defaultLatLng = new LatLng(-15.968318,-72.9574125);
//
//        CameraPosition cameraPosition = new CameraPosition.Builder()
//                .target(defaultLatLng)   //Centramos el mapa en Madrid
//                .zoom(19)         //Establecemos el zoom en 19
//                .bearing(45)      //Establecemos la orientación con el noreste arriba
//                .tilt(70)         //Bajamos el punto de vista de la cámara 70 grados
//                .build();
//
//        CameraUpdate cameraUpdate =  CameraUpdateFactory.newCameraPosition(cameraPosition);
//
//
//
//        mMap.animateCamera(cameraUpdate);


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(
                this,
                "Error de conexión con el código:" + connectionResult.getErrorCode(),
                Toast.LENGTH_LONG)
                .show();
    }

    private void manageDeniedPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            /**
             * Aquí muestras confirmación explicativa al usuario
             * por si rechazó los permisos anteriormente
             */

        } else {
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
    }

    private void updateMyLocation(Location location) {
        if (startMarker != null) {
            startMarker.remove();
            startLatLng = new LatLng(location.getLatitude(), location.getLongitude());
            startMarker = mMap.addMarker(
                    new MarkerOptions()
                            .position(startLatLng));
        } else {
            startLatLng = new LatLng(location.getLatitude(), location.getLongitude());
            startMarker = mMap.addMarker(new MarkerOptions().position(startLatLng));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLatLng, 16));
        }

    }

    private boolean isLocationPermissionGranted() {
        int permission = ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permission == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(isGoogleApiClientInitialized()){
            /**
             *Obtenemos la última ubicación al ser la primera vez
             */
            processLastLocation();
            /**
             * Iniciamos las actualizaciones de ubicación
             */
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Conexión suspendida");
        if(isGoogleApiClientInitialized())
            mGoogleApiClient.connect();

    }

    @Override
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Log.d(TAG, "Detección de actividad iniciada");

        } else {
            Log.e(TAG, "Error al iniciar/remover la detección de actividad: "
                    + status.getStatusMessage());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, String.format("Nueva ubicación: (%s, %s)",
                location.getLatitude(), location.getLongitude()));
        mLastLocation = location;
        updateMyLocation(mLastLocation);
    }

    private boolean isGoogleApiClientInitialized(){
        return mGoogleApiClient != null;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(isGoogleApiClientInitialized()){
            if (mGoogleApiClient.isConnected()) {
                stopLocationUpdates();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isGoogleApiClientInitialized()){
            if (mGoogleApiClient.isConnected() && mGoogleApiClient != null) {
                startLocationUpdates();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        /**
         * Protegemos la ubicación actual antes del cambio de configuración
         */
        outState.putParcelable(LOCATION_KEY, mLastLocation);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(TAG, "El usuario permitió el cambio de ajustes de ubicación.");
                        processLastLocation();
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.d(TAG, "El usuario no permitió el cambio de ajustes de ubicación");
                        break;
                }
                break;
        }
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                startLocationUpdates();

            } else {
                Toast.makeText(this, "Permisos no otorgados", Toast.LENGTH_LONG).show();
            }
        }
    }






}
